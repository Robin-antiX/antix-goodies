# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-01 11:32+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#: fast_personal_menu_editor.sh:11 fast_personal_menu_editor.sh:26
msgid "Fast Personal Menu Manager for IceWM"
msgstr "Fast Personal Menu Manager for IceWM"

#: fast_personal_menu_editor.sh:12
msgid "App .desktop file"
msgstr "App .desktop file"

#: fast_personal_menu_editor.sh:14
msgid "REMOVE last entry"
msgstr "REMOVE last entry"

#: fast_personal_menu_editor.sh:15
msgid "ORGANIZE entries"
msgstr "ORGANIZE entries"

#: fast_personal_menu_editor.sh:16
msgid "UNDO last change"
msgstr "UNDO last change"

#: fast_personal_menu_editor.sh:17
msgid "ADD selected app"
msgstr "ADD selected app"

#: fast_personal_menu_editor.sh:18
msgid ""
"Choose (or drag and drop to the field below) the .desktop file you want to "
"add to the personal menu \\n OR select any other option"
msgstr ""
"Choose (or drag and drop to the field below) the .desktop file you want to "
"add to the personal menu \\n OR select any other option"

#: fast_personal_menu_editor.sh:26
msgid ""
"FPM has no 'graphical' way to allow users to move icons around or delete "
"arbitrary icons.\\nIf you click OK, the personal menu configuration file "
"will be opened for editing.\\nEach menu icon is identified by a line "
"starting with 'prog' followed by the application name, icon location and the"
" application executable file.\\nMove or delete the entire line refering to "
"each personal menu entry.\\nNote: Lines starting with # are comments only "
"and will be ignored.\\nThere can be empty lines.\\nSave any changes and then"
" restart IceWM.\\nYou can undo the last change from FPMs 'Restore' button."
msgstr ""
"FPM has no 'graphical' way to allow users to move icons around or delete "
"arbitrary icons.\\nIf you click OK, the personal menu configuration file "
"will be opened for editing.\\nEach menu icon is identified by a line "
"starting with 'prog' followed by the application name, icon location and the"
" application executable file.\\nMove or delete the entire line refering to "
"each personal menu entry.\\nNote: Lines starting with # are comments only "
"and will be ignored.\\nThere can be empty lines.\\nSave any changes and then"
" restart IceWM.\\nYou can undo the last change from FPMs 'Restore' button."

#: fast_personal_menu_editor.sh:38 fast_personal_menu_editor.sh:42
#: fast_personal_menu_editor.sh:109
msgid "Warning"
msgstr "Warning"

#: fast_personal_menu_editor.sh:38
msgid "FTM is programmed to always keep 1 line in the personal menu file!"
msgstr "FTM is programmed to always keep 1 line in the personal menu file!"

#: fast_personal_menu_editor.sh:42
msgid "This will delete the last entry from your personal menu! Are you sure?"
msgstr ""
"This will delete the last entry from your personal menu! Are you sure?"

#: fast_personal_menu_editor.sh:109
msgid "No changes were made! Please choose an application."
msgstr "No changes were made! Please choose an application."
