��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     p     �     �     �     �  *   �     �     
          "                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2021
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Samodejna prijava Spremeni Zamenjava upravljalnika prijav Zamejaj ozadje Privzeti uporabnik Ob zagonu vklopi številnico na tipkovnici Upravljanik prijav Izbira teme Testna tema Preizkusi temo pre uporabo 