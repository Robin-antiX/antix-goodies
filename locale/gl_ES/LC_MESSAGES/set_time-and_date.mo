��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    3   �     �  -   �  *   "  ,   M     z     �     �     �  N   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2021
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Elixir fuso horario (usando o rato e a tecla Enter) Data: Xestionar a configuración da data e do tempo Move o control deslizante á hora correcta Move o control deslizante ao minuto correcto Saír Seleccionar a zona horaria Establecer data actual Establecer tempo actual Use o servidor de hora de Internet para configurar automaticamente a hora/data 