��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     Q     ]     e     ~     �  /   �     �     �     �     	                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2021
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Auto-acceso Cambiar Cambiar xestor de acceso Cambiar fondo Persoa usuaria por defecto Activa o bloqueo de números ao iniciar sesión Xestor de rexistros Seleccionar tema Probar temas Probar o tema antes de usalo 