��             +         �     �     �     �  (     !   4  .   V  0   �     �  	   �     �     �     �     �  	     
     D     )   \  /   �  G   �     �       7        U     j     �     �  �	  �      c     �     �     �  �  �     J  &   a  +   �  ,   �  #   �  @     C   F     �  
   �     �  <  �  !   �               /  F   D  E   �  8   �  T   
     _     {  D   �     �  !   �       
     �
    $   �#     �#     �#     �#            
                                                       	                                                                             ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose application to add to the Toolbar Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN Help::TXT Icon located! If you click 'Yes', the toolbar configuration file will be opened for manual editing.\n
How-to:\nEach toolbar icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\n Move, edit or delete the entire line referring to each toolbar icon entry.\nNote: Lines starting with # are comments only and will be ignored.\nThere can be empty lines.\nSave any changes and then restart IceWM.\nYou can undo the last change from TIMs UNDO LAST STEP button. MOVE ICON!gtk-go-back-rtl:FBTN Move Move left Move right No changes were made!\nTIP: you can always try the Advanced buttton. No icon located, using default Gears icon No localized name found, using the original one Please select any option from the buttons below to manage Toolbar icons REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop Toolbar Icon Manager UNDO LAST STEP!undo:FBTN Warning Warning::TXT What is this?\nThis utility adds and removes application icons to IceWm's toolbar.\nThe toolbar application icons are created from an application's .desktop file.\nWhat are .desktop files?\nUsually a .desktop file is created during an application's installation process to allow the system easy access to relevant information, such as the app's full name, commands to be executed, icon to be used, where it should be placed in the OS menu, etc.\nA .desktop file name usually refers to the app's name, which makes it very easy to find the intended .desktop file (ex: Firefox ESR's .desktop file is 'firefox-esr.desktop').\nWhen adding a new icon to the toolbar, the user can click the field presented in the main window and a list of all the .desktop files of the installed applications will be shown.\nThat, in fact, is a list of (almost) all installed applications that can be added to the toolbar.\nNote: some of antiX's applications are found in the sub-folder 'antiX'.\n
TIM buttons:\n 'ADD ICON' - select, from the list, the .desktop file of the application you want to add to your toolbar and it instantly shows up on the toolbar.\nIf, for some reason, TIM fails to find the correct icon for your application, it will still create a toolbar icon using the default 'gears' image so that you can still click to access the application.\nYou can click the 'Advanced' button to manually edit the relevant entry and change the application's icon.\n'UNDO LAST STEP' - every time an icon is added or removed from the toolbar, TIM creates a backup file. If you click this button, the toolbar is instantly restored from that backup file, without any confirmation.\n'REMOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to remove its icon from the toolbar\n'MOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to select it and then move it to the left or to the right\n'ADVANCED' - allows for editing the text configuration file that has all of your desktop's toolbar icon's configurations. Manually editing this file allows the user to rearrange the order of the icons and delete or add any icon. A brief explanation about the inner workings of the text configuration file is displayed before the file is opened for editing.\n Warnings: only manually edit a configuration file if you are sure of what you are doing! Always make a back up copy before editing a configuration file! You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-26 16:23+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2021
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 AGREGAR ICONA!add:FBTN CONFIGURACIÓN AVANZADA!help-hint:FBTN Engadir a icona da aplicación seleccionada Escoller a aplicación para engadir ao Panel Escoller que facer coa icona $ EXEC Faga dobre clic en calquera aplicación para mover a súa icona: Faga dobre clic en calquera aplicación para eliminar a súa icona: AXUDA!help:FBTN Axuda::TXT Icona atopada! Ao premer en 'ACEPTAR', o ficheiro de configuración "toolbar" abre para edición.\nProcedemento:\nCada icona é identificada no ficheiro por unha liña constituída por 'prog' seguido do nome do aplicativo, da localización da icona e do ficheiro executable do aplicativo.\nMover, editar ou eliminar a liña completa relativa a cada icona.\nNota: As liñas que comezan por # son só comentarios e son ignoradas.\nPode haber liñas vacías.\n Gardar calquera cambio feito e reiniciar o IceWM.\nÉ posible reverter o último cambio a través do botón 'REVERTER' do xestor. MOVER ÍCONA!gtk-go-back-rtl:FBTN Mover Mover para a esquerda Mover para a dereita Non se fixeron cmabios!\nConsello: intentar co botón 'Conf AVANZADA'. Non se atopa ningunha icona; usando a icona predeterminada Engrenaxes Non se atopou ningún nome localizado; usando o orixinal Seleccionar calquera opción dos botóns de abaixo para xestionar as iconas do panel ELIMINAR ÍCONA!remove:FBTN Eliminar Este script está destinado para executarse só nun escritorio IceWM Xestor de iconas do panel REVERTIR O ÚLTIMO PASO!undo:FBTN Aviso Aviso::TXT Que programa é este?\nEste utilitario permite engadir e eliminar iconas de aplicacións á barra de ferramentas do Panel do IceWM.\nAs iconas dos aplicativos son creados a partir do ficheiro .desktop de cada aplicativo.\nQue son ficheiros .desktop?\nPor regra, créase un ficheiro .desktop no proceso de instalación dun aplicativo, para permitir o fácil acceso do sistema a información relevante, como sexa o nome completo do aplicativo, comandos a seren executados, icona a usar, onde debe ser localizada no menu do SO, etc.\nPor regra, os nomes dos ficheiros .desktop coinciden cos nomes dos aplicativos respectivos, o que volve moi fácil atopar o ficheiro .desktop desexado (ex: o ficheiro .desktop do Firefox ESR ten o nome 'firefox-esr.desktop').\nPara engadir unha nova icona á barra de ferramentas do Panel, premer no campo 'Ficheiro .desktop do aplicativo' da lapela principal do utilitario e aparecerá unha lista dos ficheiros .desktop de todos os aplicativos instalados.\nEsta é, en verdade, unha lista de (case) todos os aplicativos instalados e cuxas iconas poden ser engadidas á barra de ferramentas.\n Nota: alguns dos aplicativos do antiX encóntranse no subcartafol 'antiX' enriba da lista.\nBotóns do xestor:\n 'ENGADIR icona' - despois de seleccionar o ficheiro .desktop do aplicativo a engadir, premer neste botón fai con que a icona apareza de inmediato na barra de ferramentas.\nSe, por calquera razón, o xestor non encontrar a icona correcta para o aplicativo a engadir, creará ainda así unha icona, usando a imaxe predefinida 'engranaxe', premendo no botón 'Conf AVANZADA'.\n'REVERTER' - de cada vez que unha icona é engadida ou eliminada da barra de ferramentas, o xestor crea unha copia de seguranza do ficheiro de configuración 'toolbar'. Premer neste botón fai con que a barra sexa instantaneamente restaurada a partir da copia de seguranza, sen necesidade de confirmación. \n 'ELIMINAR icona' - mostra unha lista de todos os aplicativos con iconas na barra de ferramentas. Premer dúas veces na parte esquerda nun aplicativo para eliminar da barra a respectiva icona\n'Conf AVANZADA' - esta opción permite editar o ficheiro de configuración que contén todas as configuracións dos ficheiros .desktop da barra de ferramentas. Editar manualmente este ficheiro posibilita ao utilizador reordenar as iconas na barra e engadir ou eliminar iconas. É presentada unha explicación breve sobre como funciona o ficheiro de configuración antes de ser aberto para edición. \n Avisos: Editar manualmente un ficheiro de configuración só se souber ben o que está a facer! Facer sempre unha copia de seguranza antes de editar un ficheiro de configuración!  Está a executar un escritorio IceWM o ficheiro ten algo o ficheiro está vacío non se seleccionou nada 