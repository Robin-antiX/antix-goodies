��            )   �      �     �     �     �  (   �  !     .   .  0   ]     �  	   �     �     �     �  	   �  
   �  D   �  )   3  /   ]  G   �     �     �  7   �     ,     A     Z     b      o     �     �     �  �  �     Q     h     �  .   �  *   �  8   �  8   0     i  
   y  
   �     �     �     �     �  A   �  ,   	  3   G	  H   {	     �	     �	  <   �	     
     ;
     V
     _
     m
     �
     �
     �
           	                                                                                       
                                       ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose application to add to the Toolbar Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN Help::TXT Icon located! MOVE ICON!gtk-go-back-rtl:FBTN Move Move left Move right No changes were made!\nTIP: you can always try the Advanced buttton. No icon located, using default Gears icon No localized name found, using the original one Please select any option from the buttons below to manage Toolbar icons REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop Toolbar Icon Manager UNDO LAST STEP!undo:FBTN Warning Warning::TXT You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-26 16:23+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 LEGG TIL IKON!add:FBTN AVANSERT!help-hint:FBTN Legg til valgt programs ikon Velg program som skal legges til verktøylinja Velg hva som skal gjøres med $EXEC-ikonet Dobbeltklikk et vilkårlig program for å flytte ikonet: Dobbeltklikk et vilkårlig program for å fjerne ikonet: HJELP!help:FBTN Hjelp::TXT Fant ikon. FLYTT IKON!gtk-go-back-rtl:FBTN Flytt Flytt til venstre Flytt til høyre Ingen endringer ble utført.\nHint: Forsøk knappen «Avansert». Fant ingen ikon. Bruker standard Gears-ikon. Fant ikke lokaltilpasset navn, bruker det originale Velg en handling fra knappene under for å behandle verktøylinjeikonene FJERN IKON!remove:FBTN Fjern Dette skriptet bør kun kjøres i et IceWM-skrivebordsmiljø Behandle verktøylinjeikoner ANGRE SISTE STEG!undo:FBTN Advarsel Advarsel::TXT Du kjører et IceWM-skrivebord filen er ikke tom filen er tom ingenting ble valgt 